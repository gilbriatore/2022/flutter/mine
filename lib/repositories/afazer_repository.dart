import 'package:mine/models/afazer_model.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

class AfazerRepository {

  Future<Afazer?> salvarAfazer(Afazer afazer) async {
    final parser = ParseObject('Afazer')
      ..set('titulo', afazer.titulo)
      ..set('situacao', afazer.situacao);

    if (afazer.id.isNotEmpty){
      parser.objectId = afazer.id;
    }

    final ParseResponse response = await parser.save();
    if (response.success && response.result != null) {
      final object = response.result as ParseObject;
      afazer.id = object.objectId!;
      return afazer;
    }
    return null;
  }

  Future<bool> excluirAfazer(Afazer afazer) async {
    var parser = ParseObject('Afazer')..objectId = afazer.id;
    final ParseResponse response = await parser.delete();
    if (response.success) {
      return true;
    }
    return false;
  }

  Future<List<Afazer>> listarAfazeres() async {

    QueryBuilder<ParseObject> queryAfazer = QueryBuilder<ParseObject>(ParseObject('Afazer'));
    final ParseResponse apiResponse = await queryAfazer.query();

    List<Afazer> listaDeAfazeres = [];
    if (apiResponse.success && apiResponse.results != null) {

      for (ParseObject object in apiResponse.results!){
        final varId = object.objectId;
        final varTitulo = object.get<String>('titulo')!;
        final varSituacao =  object.get<bool>('situacao')!;

        Afazer afazer = Afazer(varTitulo);
        afazer.id = varId!;
        afazer.situacao = varSituacao;
        listaDeAfazeres.add(afazer);
      }
      return listaDeAfazeres;
    } else {
      return [];
    }
  }
}