import 'package:flutter/material.dart';

class CadastroMetasPage extends StatefulWidget {
  const CadastroMetasPage({Key? key}) : super(key: key);
  final String title = 'Cadastrar meta';

  @override
  State<CadastroMetasPage> createState() => _CadastroMetasPageState();
}

enum Tempo {
  MES,
  ANO
}

class _CadastroMetasPageState extends State<CadastroMetasPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: [
          Column(
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 20),
                child: Text('Unidade',
                  style: TextStyle(
                      fontSize: 20
                  ),),
              ),
              Row(
                children: [
                  Radio(value: Tempo.MES,
                      groupValue: Tempo,
                      onChanged: (tempo){
                            print(tempo);
                      }
                  ),
                  Text('Mês'),
                  Radio(value: Tempo.ANO,
                      groupValue: Tempo,
                      onChanged: (tempo){
                        print(tempo);
                      }
                  ),
                  Text('Ano'),
                ],
              ),
              Container(
                margin: EdgeInsets.only(bottom: 20),
                child: const Text('Prazo',
                  style: TextStyle(
                      fontSize: 20
                  ),),
              ),
              Row(
                children: [
                  Radio(value: Tempo.MES,
                      groupValue: Tempo,
                      onChanged: (tempo){
                        print(tempo);
                      }
                  ),
                  Text('Curto'),
                  Radio(value: Tempo.ANO,
                      groupValue: Tempo,
                      onChanged: (tempo){
                        print(tempo);
                      }
                  ),
                  Text('Medio'),
                  Radio(value: Tempo.ANO,
                      groupValue: Tempo,
                      onChanged: (tempo){
                        print(tempo);
                      }
                  ),
                  Text('Longo'),
                ],
              ),
              TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Título'
                ),
              ),
              TextField(
                minLines: 2,
                maxLines: 4,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Observações'
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Cancelar'),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('OK'),
                  ),
                ],
              )
            ],
          ),

        ],
      ),
    );
  }
}
