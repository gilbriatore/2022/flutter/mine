import 'package:flutter/material.dart';

class CustosPage extends StatefulWidget {
  const CustosPage({Key? key}) : super(key: key);
  final String title = 'Custos';

  @override
  State<CustosPage> createState() => _CustosPageState();
}

class _CustosPageState extends State<CustosPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: [
          Container(
            height: 220,
            width: double.maxFinite,
            child: Card(
              child: InkWell(
                onTap: () {
                  print('Card pressionado...');
                  // Navigator.push(context,
                  //     MaterialPageRoute(builder: (context) => CustosPage())
                  // );
                },
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 20),
                      child: Text('Diário',
                        style: TextStyle(
                            fontSize: 20
                        ),),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            height: 220,
            width: double.maxFinite,
            child: Card(
              child: InkWell(
                onTap: () {
                  print('Card pressionado...');
                  // Navigator.push(context,
                  //     MaterialPageRoute(builder: (context) => CustosPage())
                  // );
                },
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 20),
                      child: Text('Mensal',
                        style: TextStyle(
                            fontSize: 20
                        ),),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            height: 220,
            width: double.maxFinite,
            child: Card(
              child: InkWell(
                onTap: () {
                  print('Card pressionado...');
                  // Navigator.push(context,
                  //     MaterialPageRoute(builder: (context) => CustosPage())
                  // );
                },
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 20),
                      child: Text('Anual',
                        style: TextStyle(
                            fontSize: 20
                        ),),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
