import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mine/models/afazer_model.dart';
import 'package:mine/stores/afazer_store.dart';

class AfazeresPage extends StatelessWidget {
  final String title = 'Afazeres';
  AfazerStore afazerStore = AfazerStore();
  final txtAfazerController = TextEditingController();

  AfazeresPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    void adicionarAfazer() async {
      if (txtAfazerController.text.trim().isEmpty) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text("Informe o afazer..."),
            duration: Duration(seconds: 2),
          ),
        );
        return;
      }
      afazerStore.salvarAfazer(txtAfazerController.text);
      txtAfazerController.clear();
      Navigator.pop(context);
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      ///Observer da lista
      body: Observer(
        builder: (_) {
          if (afazerStore.isCarregando) {
            return const Center(
              child: SizedBox(
                width: 100,
                height: 100,
                child: CircularProgressIndicator(),
              ),
            );
          }

          if (afazerStore.listaDeAfazeres.isEmpty) {
            return const Center(
              child: Text("Nenhum afazer cadastrado!",
                style: TextStyle(
                  fontSize: 20
                ),
              ),
            );
          }

          return ListView.builder(
            padding: const EdgeInsets.only(top: 10.0),
            itemCount: afazerStore.listaDeAfazeres.length,
            itemBuilder: (context, index) {
              Afazer afazer = afazerStore.listaDeAfazeres[index];

              ///Observer de cada item
              return Observer(
                builder: (_) {
                  return ListTile(
                    title: Text(afazer.titulo),
                    leading: CircleAvatar(
                      backgroundColor: afazer.situacao ? Colors.green : Colors
                          .blue,
                      foregroundColor: Colors.white,
                      child: Icon(afazer.situacao ? Icons.check : Icons.error),
                    ),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Checkbox(
                            value: afazer.situacao,
                            onChanged: (situacao) async {
                              afazer.setSituacao(situacao!);
                              afazerStore.atualizar(afazer);
                            }),
                        IconButton(
                          icon: const Icon(
                            Icons.delete,
                            color: Colors.blue,
                          ),
                          onPressed: () async {
                            afazerStore.excluirAfazer(afazer);
                          },
                        )
                      ],
                    ),
                  );
                },
              );
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add, size: 40),
        onPressed: () async {
          showDialog(
              context: context,
              builder: (context) {
                return SimpleDialog(
                  title: const Text("Cadastrar"),
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: TextField(
                        controller: txtAfazerController,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text('Cancelar'),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        ElevatedButton(
                          onPressed: () {
                            adicionarAfazer();
                          },
                          child: const Text('OK'),
                        ),
                      ],
                    )
                  ],
                );
              });
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: const BottomAppBar(
        color: Colors.blueGrey,
        child: SizedBox(height: 50),
      ),
    );
  }
}
