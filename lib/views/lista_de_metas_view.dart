import 'package:flutter/material.dart';
import 'package:mine/views/cadastro_metas_view.dart';

class ListaDeMetasPage extends StatefulWidget {
  const ListaDeMetasPage({Key? key}) : super(key: key);
  final String title = 'Metas';

  @override
  State<ListaDeMetasPage> createState() => _ListaDeMetasPageState();
}

class _ListaDeMetasPageState extends State<ListaDeMetasPage> {
  List itens = List<String>.generate(100, (index) => 'Item$index');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView.builder(
        itemCount: itens.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: 220,
            width: double.maxFinite,
            child: Card(
              child: InkWell(
                onTap: () {
                  print('Card pressionado...');
                  // Navigator.push(context,
                  //     MaterialPageRoute(builder: (context) => CustosPage())
                  // );
                },
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 20),
                      child: Text(
                        'Meta a ser alcançada...',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add, size: 40),
        onPressed: () {
          print('botão pressionado...');
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return CadastroMetasPage();
          }));
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: const BottomAppBar(
        color: Colors.blueGrey,
        child: SizedBox(height: 50),
      ),
    );
  }
}
