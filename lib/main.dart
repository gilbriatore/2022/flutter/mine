import 'package:flutter/material.dart';
import 'package:mine/views/afazeres_view.dart';
import 'package:mine/views/custos_view.dart';
import 'package:mine/views/lista_de_metas_view.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  final keyApplicationId = 'Colocar seu keyApplicationId aqui!';
  final keyClientKey = 'Colocar seu keyClientKey aqui!';
  const keyParseServerUrl = 'https://parseapi.back4app.com';

  await Parse().initialize(keyApplicationId, keyParseServerUrl,
      clientKey: keyClientKey, autoSendSessionId: true);

  // var firstObject = ParseObject('FirstClass')
  //   ..set(
  //       'message', 'Hey ! First message from Flutter. Parse is now connected');
  // await firstObject.save();
  //
  // print('done');

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'mine',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: const MyHomePage(title: 'mine'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: Drawer(),
      body: Center(
        child: GridView.count(
          primary: false,
          padding: const EdgeInsets.all(20),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          crossAxisCount: 2,
          children: <Widget>[
            Card(
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AfazeresPage())
                  );
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Icon(
                      Icons.check_box_outlined,
                      size: 80,
                    ),
                    Text('Afazeres',
                    style: TextStyle(
                      fontSize: 20
                    ),),
                  ],
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ListaDeMetasPage())
                  );
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Icon(
                      Icons.start,
                      size: 80,
                    ),
                    Text('Metas',
                      style: TextStyle(
                          fontSize: 20
                      ),),
                  ],
                ),
              ),
            ),
            Card(
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => CustosPage())
                  );
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Icon(
                      Icons.attach_money,
                      size: 80,
                    ),
                    Text('Custos',
                      style: TextStyle(
                          fontSize: 20
                      ),),
                  ],
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
